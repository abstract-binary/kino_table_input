defmodule KinoTableInput.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    Kino.SmartCell.register(KinoTableInput)
    children = []
    opts = [strategy: :one_for_one, name: KinoTableInput.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
