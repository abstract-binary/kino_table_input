defmodule KinoTableInput.MixProject do
  use Mix.Project

  @version "0.1.0"

  def project do
    [
      app: :kino_table_input,
      version: @version,
      name: "KinoTableInput",
      description: "Table input widget for Livebook",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases(),
      package: [
        files: ~w(lib .formatter.exs mix.exs README.md LICENSE doc_img),
        licenses: ["Apache-2.0"],
        links: %{"Gitlab" => "https://gitlab.com/abstract-binary/kino_table_input"}
      ],
      docs: [
        main: "readme",
        source_url: "https://gitlab.com/abstract-binary/kino_table_input",
        source_ref: "v#{@version}",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {KinoTableInput.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:explorer, "~> 0.8.0"},
      {:kino, "~> 0.12.0"},
      {:ex_doc, "~> 0.31", only: :dev, runtime: false}
    ]
  end

  defp aliases do
    [docs: ["docs", &copy_doc_imgs/1]]
  end

  defp copy_doc_imgs(_) do
    File.cp_r!("doc_img", "doc/doc_img")
  end
end
