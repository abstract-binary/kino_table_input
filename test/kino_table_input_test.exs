defmodule KinoProgressBarTest do
  use ExUnit.Case
  doctest KinoTableInput

  import Kino.Test

  setup :configure_livebook_bridge

  test "start" do
    {table, source} = Kino.Test.start_smart_cell!(KinoTableInput, %{})

    assert %{columns: _, name: "table", table: _} = connect(table)

    assert source |> String.split("\n") |> List.first() == "table ="
  end
end
